'use strict';

services.factory('Auth', function ($http, $window, $localStorage, MainService) {
	return {
		isAuthenticated: function () {
			if (typeof $localStorage.remember_auth == 'undefined') {
				return false;
			}

			if ($localStorage.remember_auth) {
				return typeof $localStorage.auth !== 'undefined';
			} else {
				return typeof $window.sessionStorage.auth !== 'undefined';
			}
		},

		login: function (credentials, remember, callback) {
			var login = $http.post(MainService.apiURL('/login'), credentials);

			var self = this;

			login.success(function (data) {
				self.completeLogin(data, remember, function () {
					return callback();
				});
			});

			return login;
		},

		logout: function () {
			delete $localStorage.auth;
			delete $localStorage.user;
			delete $localStorage.remember_auth;

			delete $window.sessionStorage.auth;
			delete $window.sessionStorage.user;
		},

		completeLogin: function (tokenData, remember, callback) {
			var storageProvider = $window.sessionStorage;

			if (remember) {
				storageProvider = $localStorage;
			}

			storageProvider.auth = JSON.stringify(tokenData);
			$localStorage.remember_auth = remember;

			if (callback) {
				return callback();
			}

			//this.getUser(function (data) {
			//	storageProvider.user = JSON.stringify(data);
			//
			//	if (callback) {
			//		callback();
			//	}
			//});
		},

		user: function () {
			if ($localStorage.remember_auth) {
				return $localStorage.user ? JSON.parse($localStorage.user) : null;
			} else {
				return $window.sessionStorage.user ? JSON.parse($window.sessionStorage.user) : null;
			}
		},

		getUser: function (callback) {
			if (!this.isAuthenticated()) {
				return null;
			}

			var authData = null;

			if ($localStorage.auth) {
				authData = $localStorage.auth;
			}

			if ($window.sessionStorage.auth) {
				authData = $window.sessionStorage.auth;
			}

			if (authData === null) {
				return null;
			}

			var tokenData = JSON.parse(authData);

			$http.get(MainService.apiURL('/users/' + tokenData.id))
				.success(function (data) {
					callback(data);
				})
				.error(function (data, status) {

				});
		}
	};
});

services.factory('AuthInterceptor', function ($q, $location, $window, $localStorage) {
	return {
		request: function (config) {
			var token;

			if ($localStorage.auth) {
				token = angular.fromJson($localStorage.auth).tokenData.token;
			}

			if ($window.sessionStorage.auth) {
				token = angular.fromJson($window.sessionStorage.auth).tokenData.token;
			}

			if (token) {
				config.headers = config.headers || {};

				config.headers.Authorization = 'Bearer ' + token;
			}

			return config;
		},

		response: function (response) {
			if (response.status === 401 || response.status === 403) {
				delete $localStorage.auth;
				delete $localStorage.user;
				delete $localStorage.remember_auth;

				delete $window.sessionStorage.auth;
				delete $window.sessionStorage.user;

				$location.path('/');
			}

			return response || $q.when(response);
		}
	}
});