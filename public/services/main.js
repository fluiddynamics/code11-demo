services.factory('MainService', function ($location) {
	return {
		apiURL: function (endpoint) {
			return '//api.' + $location.host() + endpoint;
		}
	}
});