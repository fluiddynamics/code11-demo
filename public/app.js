'use strict';

var controllers = angular.module('controllers', []);
var services = angular.module('services', []);

var app = angular.module('app', [
	'ngRoute',
	'ngStorage',
	'ngDialog',
	'angular-loading-bar',
	'controllers',
	'services'
]);

app.config([
	'$routeProvider', '$locationProvider',
	function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/login', {
				controller: 'LoginController',
				templateUrl: 'views/users/login.html',
				access: {
					requiredLogin: false
				}
			})
			.when('/', {
				controller: 'IndexController',
				templateUrl: 'views/home/index.html',
				access: {
					requiredLogin: true
				}
			})
			.when('/contacts/:id', {
				controller: 'IndexController',
				templateUrl: 'views/home/index.html',
				access: {
					requiredLogin: true
				}
			})
			.otherwise({
				redirectTo: '/'
			});

		$locationProvider.html5Mode(true);
	}
]);

app.config(function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
});

app.run(function($rootScope, $location, Auth) {
	$rootScope.$on('$routeChangeStart', function(event, nextRoute, currentRoute) {
		if (nextRoute.access && nextRoute.access.requiredLogin && !Auth.isAuthenticated()) {
			$location.path('/login');
		}
	});
});