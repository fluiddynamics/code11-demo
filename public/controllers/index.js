'use strict';

controllers.controller('IndexController', function ($scope, $http, $routeParams, $location, ngDialog, Auth, MainService) {
	$scope.contacts = [];
	$scope.groupedContacts = [];

	$scope.selectedContact = {
		_id: '',
		first_name: '',
		last_name: '',
		group: 'friends'
	};

	$scope.tempContact = {
		_id: '',
		first_name: '',
		last_name: '',
		group: 'friends'
	};

	$scope.filter = {
		name: '',
		group: ''
	};

	$scope.loadContacts = function () {
		$http.get(MainService.apiURL('/contacts'))
			.success(function (data) {
				$scope.contacts = data;

				$scope.groupContacts(data);
			})
			.error(function (data, status) {
				console.log(data);
			});
	};

	$scope.groupContacts = function (contacts) {
		$scope.groupedContacts = _.groupBy(contacts, function (item) {
			return item.first_name[0].toUpperCase();
		});
	};

	$scope.saveContact = function () {
		if ($scope.tempContact.first_name != '' && $scope.tempContact.last_name != '') {
			if ($scope.tempContact._id != '') {
				$http.put(MainService.apiURL('/contacts/' + $scope.tempContact._id), $scope.tempContact)
					.success(function () {
						$scope.loadContacts();

						$scope.selectedContact = $scope.tempContact;
					})
					.error(function (data, status) {
						console.log(data);
					});
			} else {
				$http.post(MainService.apiURL('/contacts'), $scope.tempContact)
					.success(function () {
						$scope.loadContacts();
					})
					.error(function (data, status) {
						console.log(data);
					});
			}
		}
	};

	$scope.showContactDialog = function (id) {
		if (id) {
			$scope.contacts.forEach(function (item) {
				if (id == item._id) {
					$scope.tempContact._id = item;
				}
			});
		} else {
			$scope.tempContact._id = '';
			$scope.tempContact.first_name = '';
			$scope.tempContact.last_name = '';
			$scope.tempContact.group = 'friends';
		}

		ngDialog.open({
			template: 'views/home/contact.html',
			className: 'ngdialog-theme-default',
			scope: $scope,
			closeByNavigation: true
		});
	};

	$scope.showContactDeleteDialog = function (id) {
		$scope.contacts.forEach(function (item) {
			if (id == item._id) {
				$scope.tempContact = item;
			}
		});

		ngDialog.open({
			template: 'views/home/contact_delete.html',
			className: 'ngdialog-theme-default',
			scope: $scope,
			closeByNavigation: true
		});
	};

	$scope.deleteContact = function () {
		$http.delete(MainService.apiURL('/contacts/' + $scope.tempContact._id))
			.success(function () {
				$scope.loadContacts();

				$scope.tempContact = {
					_id: '',
					first_name: '',
					last_name: '',
					group: 'friends'
				};

				$scope.selectedContact = {
					_id: '',
					first_name: '',
					last_name: '',
					group: 'friends'
				};
			})
			.error(function (data, status) {
				console.log(data);
			});
	};

	$scope.filterContacts = function () {
		var result = $scope.contacts;

		if ($scope.filter.name != '' || $scope.filter.group != '') {
			result = [];

			$scope.contacts.forEach(function (item) {
				var regex = new RegExp($scope.filter.name),
						add = false;

				if ($scope.filter.name != '') {
					if (item.first_name.match(regex) || item.last_name.match(regex)) {
						add = true;
					}
				}

				if ($scope.filter.group != '' && item.group == $scope.filter.group) {
					add = true;
				}

				if (add) {
					result.push(item);
				}
			});
		}

		$scope.groupContacts(result);
	};

	$scope.showFilterDialog = function () {
		ngDialog.open({
			template: 'views/home/filter.html',
			className: 'ngdialog-theme-default',
			scope: $scope,
			closeByNavigation: true
		});
	};

	$scope.logout = function () {
		$scope.user = null;

		Auth.logout();

		$location.path('/login');
	};

	$scope.selectedContact = {
		first_name: '',
		last_name: '',
		group: ''
	};

	if ($routeParams.id) {
		$http.get(MainService.apiURL('/contacts/' + $routeParams.id))
				.success(function (data) {
					$scope.selectedContact = data;
				})
				.error(function (data, status) {
					console.log(data);
				});
	}

	$scope.loadContacts();
});