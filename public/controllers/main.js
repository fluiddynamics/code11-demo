'use strict';

controllers.controller('MainController', function ($scope, $http, $location, Auth) {
	$scope.logout = function () {
		$scope.user = null;

		Auth.logout();

		$location.path('/login');
	};

	$scope.isAuthenticated = function () {
		return Auth.isAuthenticated();
	};

	$scope.setUser = function (user) {
		$scope.user = user;
	};

	if (Auth.isAuthenticated()) {
		$scope.setUser(Auth.user());
	}
});