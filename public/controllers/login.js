'use strict';

controllers.controller('LoginController', function ($scope, $location, Auth) {
	if (Auth.isAuthenticated()) {
		$location.path('/');
	}

	$scope.username = '';
	$scope.password = '';
	$scope.remember = true;

	$scope.submit = function () {
		var credentials = {
			email: $scope.username,
			password: $scope.password
		};

		Auth.login(credentials, $scope.remember, function () {
			$scope.setUser(Auth.user());

			$location.path('/');
		});
	};
});