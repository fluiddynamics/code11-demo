'use strict';

var mongoose = require('mongoose');

var contactSchema = mongoose.Schema({
	first_name: String,
	last_name: String,
	group: String
});

var contactModel = mongoose.model('contact', contactSchema);

module.exports = contactModel;