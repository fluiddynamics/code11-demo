'use strict';

var mongoose = require('mongoose');

var logSchema = mongoose.Schema({
	message: String,
	timestamp: Date,
	level: String,
	metadata: mongoose.Schema.Types.Mixed
});

var logModel = mongoose.model('log', logSchema);

module.exports = logModel;