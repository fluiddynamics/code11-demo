'use strict';

if (process.env.NODE_ENV && process.env.NODE_ENV == 'production') {
	require('newrelic');
}

/**
 * Setup application environment and load necessary modules.
 */
var serverPort = 3001,
	express = require('express'),
	logService = require('./services/logger'),
	db = require('./services/database'),
	bodyParser = require('body-parser'),
	jwt = require('express-jwt'),
	cors = require('cors'),
	secret = require('./config/secret'),
	app = express(),
	controllers = {},
	router = express.Router();

logService.init();

/**
 * Load application endpoints into the router.
 */
controllers.ping = require('./controllers/ping');
controllers.auth = require('./controllers/auth');
controllers.contacts = require('./controllers/contacts');

/**
 * Cross-origin resource sharing
 */
app.use('*', cors());

/**
 * Default endpoint for nosy people :)
 */
router.get('/', function (req, res) {
	res.status(200).send('This is not the page your are looking for.');
});

/**
 * Publish application endpoints.
 */
router.get('/ping', controllers.ping.get);

router.post('/login', controllers.auth.login);

router.get('/contacts', controllers.contacts.get);
router.get('/contacts/:id', controllers.contacts.getOne);
router.post('/contacts', controllers.contacts.create);
router.put('/contacts/:id', controllers.contacts.update);
router.delete('/contacts/:id', controllers.contacts.remove);

/**
 * Configure JWT
 */
var authExceptions = [
	'/',
	'/favicon.ico',
	'/ping',
	'/login'
];

app.use('/', jwt({secret: secret.secretToken}).unless({path: authExceptions}));

app.use(function (err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401).send('Invalid authentication token');
	}
});

/**
 * Load body parser to process request bodies, such as POST data.
 */
app.use('/', bodyParser({
	limit: '50mb'
}));

/**
 * Load the router.
 */
app.use('/', router);

/**
 * Connect to the database and start the application server if successful.
 */
db.connect(function () {
	app.listen(serverPort, function () {
		process.on('SIGINT', function () {
			db.disconnect();
		}).on('SIGTERM', function () {
			db.disconnect();
		});

		logService.info('Server started');
	});
});