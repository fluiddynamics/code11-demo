'use strict';

var logService = require('../services/logger'),
	contactModel = require('../models/contact');

module.exports.get = function (req, res) {
	contactModel.find({})
		.sort('first_name')
		.sort('last_name')
		.exec(function (err, items) {
			if (err) {
				logService.error('db', new Error(err));

				return res.status(500).send('Database error');
			}

			return res.status(200).json(items);
		});
};

module.exports.getOne = function (req, res) {
	contactModel.findOne({
		_id: req.params.id
	}, function (err, item) {
		if (err) {
			logService.error('db', new Error(err));

			return res.status(500).send('Database error');
		}

		return res.status(200).json(item);
	});
};

module.exports.create = function (req, res) {
	var contact = new contactModel();

	contact.first_name = req.body.first_name;
	contact.last_name = req.body.last_name;
	contact.group = req.body.group;

	contact.save(function (err) {
		if (err) {
			logService.error('db', new Error(err));

			return res.status(500).send('Database error');
		}

		return res.status(200).send('OK');
	});
};

module.exports.update = function (req, res) {
	contactModel.update({
		_id: req.params.id
	}, {
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		group: req.body.group
	}, function (err) {
		if (err) {
			logService.error('db', new Error(err));

			return res.status(500).send('Database error');
		}

		return res.status(200).send('OK');
	});
};

module.exports.remove = function (req, res) {
	if (typeof req.params.id == 'undefined' || req.params.id == '') {
		return res.send(400);
	}

	contactModel.remove({
		_id: req.params.id
	}, function (err) {
		if (err) {
			logService.error('db', new Error(err));

			return res.status(500).send('Database error');
		}

		return res.status(200).send('OK');
	});
};