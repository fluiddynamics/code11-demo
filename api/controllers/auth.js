'use strict';

var loginService = require('../services/auth/login');

module.exports.login = function (req, res) {
	loginService.localLogin(req.body.email, req.body.password, function (err, token) {
		if (err) {
			if (err.code) {
				return res.status(err.code).send(err.message);
			}

			return res.status(500).json(err);
		}

		return res.status(200).json(token);
	});
};