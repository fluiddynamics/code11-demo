'use strict';

/**
 * Set and OK header to confirm application availability.
 *
 * @param req
 * @param res
 * @returns {*}
 */
module.exports.get = function (req, res) {
	return res.status(200);
};