'use strict';

var mongoose = require('mongoose'),
	dbConfig = require('../config/database.js').config(),
	logService = require('../services/logger.js');

mongoose.connection.on('error', function(err) {
	logService.error('db', new Error(err));
});

mongoose.connection.on('disconnected', function () {
	logService.info('Disconnected from ' + dbConfig.dbUri);
});

var db = {
	attachmentDb: null,

	connect: function (callback) {
		mongoose.connect(dbConfig.dbUri);

		mongoose.connection.once('open', function () {
			logService.info('Connected to ' + dbConfig.dbUri);

			return callback();
		});
	},

	disconnect: function () {
		mongoose.connection.close(function () {
			process.exit(0);
		});
	}
};

module.exports = db;