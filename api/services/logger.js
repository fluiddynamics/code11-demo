'use strict';

var winston = require('winston'),
	logConfig = require('../config/log.js'),
	dbConfig = require('../config/database.js').config(),
	MongoDB = require('winston-mongodb').MongoDB,
	logModel = require('../models/log.js');

module.exports.init = function (collection) {
	if (!collection) {
		collection = 'logs';
	}

	winston.add(MongoDB, {
		dbUri: dbConfig.dbUri,
		db: dbConfig.dbUri,
		collection: collection,
		handleExceptions: true,
		exitOnError: false,
		level: 'debug'
	});

	if (process.env.NODE_ENV && process.env.NODE_ENV == 'production') {
		winston.remove(winston.transports.Console);
	}
};

module.exports.info = function (data) {
	if (logConfig.log_levels.info === false) {
		return;
	}

	winston.info(data);
};

module.exports.warn = function (group, err, extra) {
	if (logConfig.log_levels.warn === false) {
		return;
	}

	var data = {
		stack: err.stack
	};

	if (extra) {
		data.extra = extra;
	}

	winston.warn(group, data);
};

module.exports.error = function (group, err, extra) {
	if (logConfig.log_levels.error === false) {
		return;
	}

	var data = {
		stack: err.stack
	};

	if (extra) {
		data.extra = extra;
	}

	winston.error(group, data);
};

module.exports.debug = function (group, msg, extra) {
	if (logConfig.log_levels.debug === false) {
		return;
	}

	var data = {
		message: msg
	};

	if (extra) {
		data.extra = extra;
	}

	winston.debug(group, data);
};

module.exports.cleanup = function (levels, callback) {
	var self = this;

	if (!levels) {
		levels = ['info'];
	}

	logModel.remove({
		level: {
			$in: levels
		}
	}, function (err) {
		if (err) {
			self.error('db', new Error(err));

			if (callback) {
				return callback(err);
			}
		}

		if (callback) {
			return callback();
		}
	});
};