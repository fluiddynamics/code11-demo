'use strict';

var tokenService = require('../../services/auth/token.js');

module.exports.localLogin = function (email, password, callback) {
	if (!email || !password) {
		return callback({
			code: 401,
			message: 'Invalid credentials'
		});
	}

	// do login stuffs here

	var tokenData = tokenService.generate({
		id: 'some_id',
		email: email
	});

	return callback(null, tokenData);
};