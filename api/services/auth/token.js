var jwt = require('jsonwebtoken'),
	secret = require('../../config/secret'),

	TOKEN_EXPIRATION = 60 * 24 * 30,
	TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60,
	TOKEN_EXPIRATION_MSEC = TOKEN_EXPIRATION_SEC * 1000;

/**
 * Generate JWT token for the specified user.
 *
 * @param user
 */
module.exports.generate = function (user) {
	return generateToken(user);
};

/**
 * Check JWT token validity.
 *
 * @param token
 * @param callback
 */
module.exports.check = function (token, callback) {
	jwt.verify(token, secret.secretToken, function (err, user) {
		if (err) {
			return callback(err);
		}

		return callback(false, generateToken(user));
	});
};

/**
 * Generate JWT token.
 *
 * @param user
 * @returns {{id: *, tokenData: {token: *, refreshToken: *, expiration: Date}}}
 */
function generateToken (user) {
	var token = jwt.sign({
		id: user.id,
		email: user.email,
		timestamp: new Date()
	}, secret.secretToken, {
		expiresIn: TOKEN_EXPIRATION_SEC
	});

	var expiration = new Date();
	expiration.setTime(expiration.getTime() + TOKEN_EXPIRATION_MSEC);

	return {
		id: user.id,
		tokenData: {
			token: token,
			expiration: expiration
		}
	};
}