'use strict';

module.exports = {
	log_levels: {
		info: true,
		debug: false,
		warn: true,
		error: true
	}
};