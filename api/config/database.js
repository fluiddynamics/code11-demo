'use strict';

var config = {
	development: {
		dbUri: 'mongodb://localhost/code11',
		dbName: 'code11'
	},
	production: {
		dbUri: '',
		dbName: ''
	}
};

module.exports.config = function () {
	if (process.env.NODE_ENV) {
		return config[process.env.NODE_ENV];
	}

	return config['development'];
};